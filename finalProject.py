import math
TRAIN_FILE_NAME = "HAM-Train.txt"
# TEST_FILE_NAME = "HAM-Test.txt"
TEST_FILE_NAME = "custom_test.txt"
START_SYMBOL = "<شروع>"
BACK_OFF_PARAMETER = 0.8

languages = []
train_words = []
word_counter = []
word_counter_bigram = []
p_language = []
p_uni = [dict()]
p_bi = [dict()]

predict_language_test = []
actual_language_test = []
test_words = []


def combine(str1, str2):
    return str1 + "&" + str2


def read_test_data():
    global actual_language_test
    global test_words
    print("Reading Test Data...")

    test_data_file = open(TEST_FILE_NAME, "r", encoding="utf-8")
    test_data = test_data_file.read()

    new_text = True
    new_word = ""

    for c in test_data:
        if c == '@':
            if new_text:
                actual_language_test.append(languages.index(new_word))
                test_words.append([])
                test_words[len(test_words)-1].append(START_SYMBOL)
                new_text = False
                new_word = ""
        elif new_text:
            new_word += c
        elif c == ' ':
            if new_word != "":
                test_words[len(test_words)-1].append(new_word)
            new_word = ""
        elif c == '\n':
            if new_word != "":
                test_words[len(test_words)-1].append(new_word)
            new_word = ""
            new_text = True
        else:
            new_word += c


def calculate_test_language_unigram():
    global languages
    global p_language
    global test_words
    global p_uni
    global word_counter
    global predict_language_test

    print("Calculate Test Languages Unigram...")
    predict_language_test.clear()
    num_found = 0
    num_not = 0
    for text in range(len(test_words)):
        maxim = float('-inf')
        arg_maxim = -1

        for language in range(len(languages)):
            p = p_language[language]
            for word in test_words[text]:
                if word != START_SYMBOL:
                    if word in p_uni[language]:
                        p += math.log(p_uni[language].get(word))
                        num_found = num_found + 1
                    else:
                        num_not = num_not + 1
                        laplas_p = 1 / (len(train_words[language]) + 2)
                        p += math.log(laplas_p)

            if p > maxim:
                maxim = p
                arg_maxim = language
        predict_language_test.append(arg_maxim)

    print("Found : " + str(num_found))
    print("Not Found: " + str(num_not))


def calculate_test_language_bigram():
    global languages
    global p_language
    global test_words
    global p_bi
    global word_counter
    global word_counter_bigram
    global predict_language_test

    print("Calculate Test Languages Bigram...")
    predict_language_test.clear()

    num_found = 0
    num_not = 0
    for text in range(len(test_words)):
        maxim = float('-inf')
        arg_maxim = -1

        for language in range(len(languages)):
            p = p_language[language]
            for i in range(len(test_words[text])):
                word = test_words[text][i]
                if word != START_SYMBOL:
                    word_prev = test_words[text][i-1]
                    combine_word = combine(word_prev, word)
                    if combine_word in p_bi[language]:
                        p += math.log(p_bi[language].get(combine_word))
                        num_found = num_found + 1
                    else:
                        if word in word_counter[language] and BACK_OFF_PARAMETER<1:
                            p2 = 0 + (1-BACK_OFF_PARAMETER)*(word_counter[language][word] / len(train_words[language]))
                            num_found = num_found + 1
                        elif word_prev in word_counter[language]:
                            p2 = 1 / (word_counter[language][word_prev] + 2)
                            num_not = num_not + 1
                        else:
                            p2 = 1 / (len(train_words[language]) + 2)
                            num_not = num_not + 1
                        p += math.log(p2)

            if p > maxim:
                maxim = p
                arg_maxim = language
        predict_language_test.append(arg_maxim)
    print("Found : " + str(num_found))
    print("Not Found: " + str(num_not))


def evaluate():
    global actual_language_test
    global predict_language_test
    print("Evaluating...")

    actual_predict = []
    for i in range(len(languages)):
        actual_predict.append([0]*len(languages))

    for i in range(len(actual_language_test)):
        actual_predict[actual_language_test[i]][predict_language_test[i]] =\
            actual_predict[actual_language_test[i]][predict_language_test[i]] + 1

    for i in range(len(languages)):
        sum_row = 0
        sum_column = 0
        for j in  range(len(languages)):
            sum_row += actual_predict[i][j]
            sum_column += actual_predict[j][i]
        precition = actual_predict[i][i] / sum_column
        recall = actual_predict[i][i] / sum_row
        f_measure = (precition*recall*2) / (precition+recall)
        print(languages[i])
        print("precition = " + str(precition*100)+"%")
        print("recall    = " + str(recall*100)+"%")
        print("F Score   = " + str(f_measure*100) + "%\n")


def read_train_data():
    global train_words
    global languages

    print("Reading Train Data...")
    train_data_file = open(TRAIN_FILE_NAME, "r", encoding="utf-8")
    train_data = train_data_file.read()

    new_text = True
    new_word = ""
    current_language = 0
    for c in train_data:
        if c == '@':
            if new_text:
                if new_word not in languages:
                    languages.append(new_word)
                    train_words.append([])
                current_language = languages.index(new_word)
                train_words[current_language].append(START_SYMBOL)
                new_text = False
                new_word = ""
        elif new_text:
            new_word += c
        elif c == ' ':
            if new_word != "":
                train_words[current_language].append(new_word)
            new_word = ""
        elif c == '\n':
            if new_word != "":
                train_words[current_language].append(new_word)
            new_word = ""
            new_text = True
        else:
            new_word += c


def calculate_probability_languages():
    global p_language
    print("Calculating Probability Languages...")

    total_words = 0
    for l in range(len(languages)):
        total_words += len(train_words[l])
    for l in range(len(languages)):
        p_language.append(len(train_words[l])/total_words)


def create_bigram_model():
    global languages
    global train_words
    global word_counter
    global word_counter_bigram
    global p_bi
    print("Creating Bigram Model...")

    for l in range(len(languages)):
        p_bi.append(dict())
        for i in range(len(train_words[l])):
            word = train_words[l][i]
            if word != START_SYMBOL:
                word_prev = train_words[l][i-1]
                combine_word = combine(word_prev, word)
                if combine_word not in p_bi[l]:
                    p_bi_complex = word_counter_bigram[l][combine_word]/word_counter[l][word_prev]
                    p_bi_simple = word_counter[l][word] / len(train_words[l])
                    p_bi[l][combine_word] = BACK_OFF_PARAMETER*p_bi_complex + (1-BACK_OFF_PARAMETER)*p_bi_simple


def create_unigram_model():
    global languages
    global train_words
    global word_counter
    global p_uni
    print("Creating Unigram Model...")

    for l in range(len(languages)):
        p_uni.append(dict())
        for word in train_words[l]:
            if word not in p_uni[l]:
                p_uni[l][word] = word_counter[l][word] / len(train_words[l])


def count_words():
    global languages
    global train_words
    global word_counter
    global word_counter_bigram

    print("Counting Words...")
    for l in range(len(languages)):
        word_counter.append({})
        word_counter_bigram.append({})
        for i in range(len(train_words[l])):
            word = train_words[l][i]
            if word in word_counter[l]:
                word_counter[l][word] = word_counter[l][word] + 1
            else:
                word_counter[l][word] = 1

            if word != START_SYMBOL:
                word_prev = train_words[l][i-1]
                combine_word = combine(word_prev, word)
                if combine_word in word_counter_bigram[l]:
                    word_counter_bigram[l][combine_word] = word_counter_bigram[l][combine_word] + 1
                else:
                    word_counter_bigram[l][combine_word] = 1


def solve():
    read_train_data()
    calculate_probability_languages()
    count_words()
    create_unigram_model()
    create_bigram_model()
    read_test_data()
    print("\nUnigram:")
    calculate_test_language_unigram()
    evaluate()
    print("\nBigram:")
    calculate_test_language_bigram()
    evaluate()


solve()